var m = require("mithril");

var { Header, HeaderLink, Tabs, Tab,
TabContent, Content, List, Button, FloatingButton,
Accordian, Spinner,
ProgressBar, Card, CardContent,
CardAction } = require('./components/PhononComponents');

var Home = {
    progress:0,
    oncreate:function(){
        phonon.options({
            navigator: {
                defaultPage: 'home',
                animatePages: true,
                enableBrowserBackButton: true,
                templateRootDirectory: './tpl'
            },
            i18n: null // for this example, we do not use internationalization
        });
        
        
        var app = phonon.navigator();
       
        /**
         * The activity scope is not mandatory.
         * For the home page, we do not need to perform actions during
         * page events such as onCreate, onReady, etc
        */
        app.on({page: 'home', preventClose: false, content: null});
        
        app.start();
        
     },
    view:function(vnode){
        return [
                m('Home', {'data-page':'true'}, [
                    m(Header, {title:'Hello'}, [
                        m(HeaderLink, {dir:'left', icon:'icon-home', onclickLeft:function(){}}),
                        m(HeaderLink, {dir:'right', icon:'icon-menu', onclickRight:function(){}}),
                    ]),
                    m(Tabs, [
                        m(Tab, 'a'),
                        m(Tab, {icon:'icon-edit'}),
                        m(Tab, {icon:'icon-settings'}, 'c'),
                    ]),
                    m(Content, {'data-tab-contents':'true', 'data-disable-swipe':'false', 'data-tab-default':1}, 
                        [
                            m(TabContent, [
                                m(List, {source:['hello', 'aaa', 'ccc','dd'], class:'padded-list', divider:'List'}),
                                m(List, {divider:'Item'}, [
                                    [
                                        m('a.pull-left.icon.icon-edit'),
                                        m('span', 'item')
                                    ],
                                    [
                                        m('a.pull-right.icon.icon-edit'),
                                        m('span.padded-list', 'item 2')
                                    ],
                                ]),
                                 m(List, {divider:'Accordian'}, [
                                    [
                                        m(Accordian, {href:'#Link1', title:'ini title'}, 'ini isi...')
                                    ],
                                    [   
                                        m('a.pull-right.icon.icon-edit'),
                                        m('span.padded-list', 'item 2')
                                    ]


                                ]) 
                            ]),
                            m(TabContent, [
                                m('.Padded-full', [
                                    m('h1', 'What do you want?'),
                                    m(Button, {onclick:function(){
                                        Home.progress += 10;
                                    }, type:'Positive'}, 'Add!!'),
                                    m(Button, {onclick:function(){
                                        Home.progress -= 10;
                                    },type:'negative'}, 'Substract!!'),
                                    m(Spinner),
                                    m(ProgressBar, {value:Home.progress}),
                                    m(FloatingButton, {class:'icon-edit bottom active primary'})
                                ])

                            ]),
                            m(TabContent, [
                                m('.Padded-full', [
                                    m('h2', 'Float Button?'),
                                    m('p', 'Hello there, adssadas'),
                                    m(Card, {title:'This is Title'}, 
                                        m(CardContent, [
                                            m('p', 'I am a very simple card. I am good at containing small bits of information. I am convenient because I require little markup to use effectively.'),
                                        ]),
                                        m(CardAction, [
                                            m('a', 'This is Link 1'),
                                            m('a', 'This is Link 2'),
                                        ])
                                    )
                                ]),
                            ]),
                        ])
                ])
            ]
    }
}

var PageTwo = {
    view:function(){
        return  m('.page.cached.navbar-fixed', {'data-page':'about'},
                    m(Navbar, [
                    m(NavbarLeft, ''),
                    m(NavbarCenter, {title:'Mithril F7'}),
                    m(NavbarRight, '')]),
                    m(Toolbar, {tabbar:true}, [
                        m(Link, {href:'#tab1', active:true, tabLink:true}, 'aaa'),
                        m(Link, {href:'#tab2', active:true, tabLink:true}, 'aaa'),
                    ]),
                    m(Tabs, {animated:true}, [
                        m(Tab, {id:'tab1'}, 'aaa'),
                        m(Tab, {id:'tab2'}, 'aaa'),
                    ])
                )

    }
}

module.exports = Home;