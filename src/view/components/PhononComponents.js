var m = require('mithril');
var HyperList = require('hyperlist');

var Header = {
    view:function(vnode){
        return m('header.header-bar', vnode.attrs, m('.center', [vnode.children, m('h1.title', vnode.attrs.title)]));
    }
    
}

var HeaderLink = {
    view:function(vnode){

        var addingClass = (vnode.attrs.dir === 'left') ? '.pull-left' : '.pull-right';
        var addIcon =  addingClass + '.icon' + '.' + vnode.attrs.icon;
        (vnode.attrs.icon) ? false : addIcon = '';
        return m('a.btn'+addingClass+addIcon, vnode.attrs, vnode.children);
    }
}

var Tabs = {
    view:function(vnode){
        var addHeaderClass = '.header-tabs';
        (vnode.attrs.bottom === undefined || vnode.attrs.bottom === false) ? addHeaderClass = '.header-tabs' : addHeaderClass = '';
        return (vnode.attrs.bottom === undefined || vnode.attrs.bottom === false) ? 
                m('nav.tabs'+addHeaderClass, vnode.attrs, [ m('.tab-items', vnode.children), m('.tab-indicator') ]) //put tab indicator at bottom for header
                 :  m('nav.tabs'+addHeaderClass, vnode.attrs, [ m('.tab-indicator') ], m('.tab-items', vnode.children)); //put tab indicator at header for bottom
    }
}

var Tab = {
    view:function(vnode){
        var addIcon =  '.icon' + '.' + vnode.attrs.icon;
        var addText = '.icon-text';
        (vnode.attrs.icon === undefined) ? addIcon = '' : false;
        (vnode.children.length > 0) ? true : addText = '';

        return (vnode.attrs.icon === undefined) ? 
                m('a.tab-item', vnode.attrs, vnode.children) 
                : m('a.tab-item'+addText, vnode.attrs, m('i.'+addIcon),  vnode.children);
    }
}

var Content = {
    view:function(vnode){
        return m('.content', vnode.attrs, vnode.children);
    }
}

var TabContent = {
    view:function(vnode){
        return  m('.tab-content', vnode.attrs, vnode.children);
    }
}

var List = {
    view:function(vnode){

        var dataList = vnode.attrs.source;
        (dataList === undefined) ? dataList = [] : dataList;

        return (dataList.length > 0) ? 
            m('ul.list', vnode.attrs, [(vnode.attrs.divider !== undefined) ? m('li.divider', vnode.attrs.divider) : false, dataList.map(function(value, index){
                    return m('li', m('a', vnode.attrs, value));
                })
        ]) :  (vnode.attrs.divider !== undefined) ?  m('ul.list', [
                m('li.divider', vnode.attrs.divider),
                 vnode.children.map(function(val, index){
                    return m('li', vnode.attrs, val)
                })                
            ]) :  m('ul.list', vnode.children.map(function(val, index){
                    return m('li', vnode.attrs, val)
                }))
            
    }
}

var SearchBar = {
    view:function(vnode){
        return m('header.header-bar', vnode.attrs, [
                m('a.btn.pull-left.icon.icon-close', {onclick:vnode.attrs.onclickCancel}),
                m('input.pull-left.search-input', {type:'text', placeholder:vnode.attrs.placeholder}),
                m('a.btn.pull-right.icon.icon-check', {onclick:vnode.attrs.onclickCheck}),
        ]);
    }
}

var Button = {
    view:function(vnode){
        var type = '';
        (vnode.attrs.type) ? type = vnode.attrs.type : type = '';
        return m('button.btn.'+type, vnode.attrs, vnode.children)
    }
}

var FloatingButton = {
    view:function(vnode){
        var position = '';
        (vnode.attrs.position) ? position = vnode.attrs.position : position = '';
        return m('button.floating-action.padded-full.icon.'+position, vnode.attrs, vnode.children)
    }
}

var Accordian = {
    view:function(vnode){
        var position = '';
        (vnode.attrs.position) ? position = vnode.attrs.position : position = '';
        return [
            m('i.pull-right.icon.icon-expand-more'),
            m('a', {href:vnode.attrs.href, class:'padded-list'}, vnode.attrs.title),
            m('.accordion-content', vnode.children)
        ]
    }
}

var Card = {
    view:function(vnode){

        return m('.card',  vnode.attrs, [
            m('h3.card-content',  [
                vnode.attrs.title, 
                //m('i.pull-right.icon.icon-more-vert')
            ]),
            vnode.children
        ])
    }
}

var CardContent = {
    view:function(vnode){
        return m('.card-content', vnode.attrs, vnode.children)
    }
}

var CardAction = {
    view:function(vnode){
        return m('.card-action', vnode.attrs, vnode.children)
    }
}

var Spinner = {
    view:function(vnode){
        return m('.circle-progress.active', m('.spinner'))
    }
}

var ProgressBar  = {
    view:function(vnode){
        (vnode.attrs.value === undefined) ? vnode.attrs.value = 0 : false;
        return m('.progress.active', m('.determinate', {style:'width:'+vnode.attrs.value+'%'}))
    }
}

var Panel = {
    view:function(vnode){
        return m('.panel', vnode.attrs, [
                 m('.header-bar', [
                    m('.center', 
                        m('h5', {style:'color:white'},vnode.attrs.title)),
                    m(HeaderLink, {'data-panel-close':true, dir:'right', icon:'icon-close', onclickRight:function(){}})
                 ]),
                m('.content', vnode.children)
        ])
    }
}

var VirtualList = {
    oninit:function(){
        this.idRand = Math.floor(Math.random() * 1000);
    },
    oncreate:function(vnode){
        
        const {rows, size, config, generateHeightTemplateItem, onclick} = vnode.attrs;
        const container = document.getElementById('container-list-'+vnode.state.idRand);
        
        generateHeightTemplateItem(size, container, rows);
        const list = HyperList.create(container, config);
        
        /* const generateTempElement = (size) => {
            for(let i = 0; i < size; i++)
            {
                const el = document.createElement('li');                
                //el.setAttribute('id', 'ayah-'+i);
                el.setAttribute('data-index', i);
                el.innerHTML = child;
                el.style.width = '100%';
                //el.style.visibility = "hidden";
                container.appendChild(el);
                rows.push(Object.assign({}, {height:el.offsetHeight}));
                
                container.removeChild(el);
                console.log(rows[i]);
            }
        } */

        


            
            
        
    },
    view:function(vnode){
        return m('ul.list', {id:'container-list-'+vnode.state.idRand})
    }
}

module.exports = { 
    Header, HeaderLink, Tabs, Tab,
    Content, TabContent, List, SearchBar,
    Button, FloatingButton, Accordian,
    Spinner, ProgressBar, Card, CardContent,
    CardAction, Panel, VirtualList
}