var m = require('mithril');

var QuranAPI = {
    urlAPI:"",
    isLoading:true,
    isScroll:false,
    listAyat: [
            "Surat Al Fatihah (Pembukaan)",
            "Surat Al Baqarah (Sapi Betina)",
            "Surat Ali 'Imran (Keluarga 'Imran)",
            "Surat An Nisa' (Wanita)",
            "Surat Al Ma'idah (Hidangan)",
            "Surat Al An'am (Binatang Ternak)",
            "Surat Al A'raf  (Tempat Tertinggi)",
            "Surat Al Anfal (Rampasan Perang)",
            "Surat At Taubah (Pengampunan)",
            "Surat Yunus (Nabi Yunus A.S.)",
            "Surat Hud (Nabi Huud A.S.)",
            "Surat Yusuf (Nabi Yusuf A.S.)",
            "Surat Ar Ra'd (Guruh)",
            "Surat Ibrahim (Nabi Ibrahim A.S.)",
            "Surat Al Hijr (Daerah Pegunungan)",
            "Surat An Nahl (Lebah)",
            "Surat Al Israa' (Memperjalankan Di Malam Hari)",
            "Surat Al Kahfi (Gua)",
            "Surat Maryam (Maryam)",
            "Surat Thaha (Thaahaa)",
            "Surat Al Anbiya' (Kisah Para Nabi)",
            "Surat Al Hajj (Ibadah Haji)",
            "Surat Al Mu'minun (Orang Mukmin)",
            "Surat An Nur (Cahaya)",
            "Surat Al Furqaan (Pembeda)",
            "Surat Asy Syu'ara' (Penyair)",
            "Surat An Naml (Semut)",
            "Surat Al Qashash (Cerita)",
            "Surat Al 'Ankabuut (Laba-Laba)",
            "Surat Ar Ruum (Bangsa Rumawi)",
            "Surat Luqman (Luqman)",
            "Surat As Sajdah ((Sujud)",
            "Surat Al Ahzab (Golongan Yang Bersekutu)",
            "Surat Saba' (Kaum Saba')",
            "Surat Fathir (Pencipta)",
            "Surat Yaasiin",
            "Surat Ash Shaffat (Yang Bershaf-Shaf)",
            "Surat Shaad",
            "Surat Az Zumar (Rombongan-Rombongan)",
            "Surat Al Mu'min (Orang Yang Beriman)",
            "Surat Fushshilat (Yang Dijelaskan)",
            "Surat Asy Syuura (Musyawarah)",
            "Surat Az Zukhruf (Perhiasan)",
            "Surat Ad Dukhaan (Kabut)",
            "Surat Al Jaatsiyah (Yang Berlutut)",
            "Surat Al Ahqaaf (Bukit Pasir)",
            "Surat Muhammad (Nabi Muhammad SAW)",
            "Surat Al Fath (Kemenangan)",
            "Surat Al Hujuraat (Kamar-Kamar)",
            "Surat Qaaf",
            "Surat Adz Dzaariyaat (Angin Yang Menerbangkan)",
            "Surat Ath Thuur (Bukit)",
            "Surat An Najm (Bintang)",
            "Surat Al Qamar (Bulan)",
            "Surat Ar Rahmaan (Yang Maha Pemurah)",
            "Surat Al Waaqi'ah (Hari Kiamat)",
            "Surat Al Hadid (Besi)",
            "Surat Al Mujadilah (Wanita Yang Mengajukan Gugatan)",
            "Surat Al Hasyr (Pengusiran)",
            "Surat Al Mumtahanah (Wanita Yang Diuji)",
            "Surat Ash Shaff (Barisan)",
            "Surat Al Jumu'ah (Hari Jum'at)",
            "Surat Al-Munafiqun (Orang-Orang Munafik)",
            "Surat At Taghabun (Hari Ditampakkan Kesalahan-Kesalahan)",
            "Surat Ath Thalaaq (Talak)",
            "Surat At Tahrim (Mengharamkan)",
            "Surat Al Mulk (Kerajaan)",
            "Surat Al Qalam (Pena)",
            "Surat Al Haqqah (Kiamat)",
            "Surat Al Ma'arij (Tempat-Tempat Naik)",
            "Surat Nuh (Nabi Nuh A.S)",
            "Surat Al Jin (Jin)",
            "Surat Al Muzzammil (Orang Yang Berselimut)",
            "Surat Al Muddatstsir (Orang Yang Berselimut)",
            "Surat Al Qiyamah (Hari Kiamat)",
            "Surat Al Insaan (Manusia)",
            "Surat Al Mursalat (Malaikat-Malaikat Yang Diutus)",
            "Surat An Naba´ (Berita Besar)",
            "Surat An Naazi´ (Malaikat-Malaikat Yang Mencabut)",
            "Surat 'Abasa (Bermuka Masam)",
            "Surat At Takwir (Menggulung)",
            "Surat Al Infithar (Terbelah)",
            "Surat Al Muthaffifiin (Orang-Orang Yang Curang)",
            "Surat Al Insyiqaaq (Terbelah)",
            "Surat Al Buruuj (Gugusan Bintang)",
            "Surat Ath Thaariq (Yang Datang Di Malam Hari)",
            "Surat Al A´Laa (Yang Paling Tinggi)",
            "Surat Al Ghaasyiyah (Hari Kiamat)",
            "Surat Al Fajr (Fajar)",
            "Surat Al Balad (Negeri)",
            "Surat Asy Syams (Matahari)",
            "Surat Al Lail (Malam)",
            "Surat Adh Dhuhaa (Waktu Dhuha)",
            "Surat Alam Nasyrah /Al Insyirah (Bukankah Kami Telah Melapangkan)",
            "Surat At Tiin (Buah Tin)",
           " Surat Al 'Alaq (Segumpal Darah)",
            "Surat Al Qadr (Kemuliaan)",
            "Surat Al Bayyinah (Bukti Yang Nyata)",
            "Surat Al Zalzalah (Goncangan)",
            "Surat Al 'Adiyat (Kuda Perang Yang Berlari Kencang)",
            "Surat Al Qari'ah (Hari Kiamat)",
            "Surat At Takatsur (Bermegah-Megahan)",
            "Surat Al 'Ashr (Masa)",
            "Surat Al Humazah (Pengumpat)",
            "Surat Al Fiil (Gajah)",
            "Surat Quraisy (Suku Quraisy)",
            "Surat Al Ma'un (Barang-Barang Yang Berguna)",
            "Surat Al Kautsar (Nikmat Yang Banyak)",
            "Surat Al Kafirun (Orang-Orang Kafir)",
            "Surat An Nashr (Pertolongan)",
            "Surat Al Lahab (Gejolak Api)",
            "Surat Al Ikhlas (Memurnikan Keesaan Allah)",
            "Surat Al Falaq (Waktu Subuh)",
            "Surat An Naas (Manusia)"
        ],
    listJuz:[
        'Juz 1 Dari Al - Fatihah Ayat 1',
        'Juz 2 Dari Al - Fatihah Ayat 1',
        'Juz 3 Dari Al - Fatihah Ayat 1',
        'Juz 4 Dari Al - Fatihah Ayat 1',
        'Juz 5 Dari Al - Fatihah Ayat 1',
        'Juz 6 Dari Al - Fatihah Ayat 1',
        'Juz 7 Dari Al - Fatihah Ayat 1',
        'Juz 8 Dari Al - Fatihah Ayat 1',
        'Juz 9 Dari Al - Fatihah Ayat 1',
        'Juz 10 Dari Al - Fatihah Ayat 1',
        'Juz 11 Dari Al - Fatihah Ayat 1',
        'Juz 12 Dari Al - Fatihah Ayat 1',
        'Juz 13 Dari Al - Fatihah Ayat 1',
        'Juz 14 Dari Al - Fatihah Ayat 1',
        'Juz 15 Dari Al - Fatihah Ayat 1',
        'Juz 16 Dari Al - Fatihah Ayat 1',
        'Juz 17 Dari Al - Fatihah Ayat 1',
        'Juz 18 Dari Al - Fatihah Ayat 1',
        'Juz 19 Dari Al - Fatihah Ayat 1',
        'Juz 20 Dari Al - Fatihah Ayat 1',
        'Juz 21 Dari Al - Fatihah Ayat 1',
        'Juz 22 Dari Al - Fatihah Ayat 1',
        'Juz 23 Dari Al - Fatihah Ayat 1',
        'Juz 24 Dari Al - Fatihah Ayat 1',
        'Juz 25 Dari Al - Fatihah Ayat 1',
        'Juz 26 Dari Al - Fatihah Ayat 1',
        'Juz 27 Dari Al - Fatihah Ayat 1',
        'Juz 28 Dari Al - Fatihah Ayat 1',
        'Juz 29 Dari Al - Fatihah Ayat 1',
        'Juz 30 Dari Al - Fatihah Ayat 1',
    ],
    listHadis: [
        {title:'ADAB-ADAB KETIKA MENGUAP', content:'Dari Abu Hurairah radhiallahu ‘anhu dari Nabi shallallahu ‘alaihi wasallam: "Sesungguhnya Allah menyukai amalan bersin dan membenci menguap. Karenanya apabila salah seorang dari kalian bersin lalu dia memuji Allah, maka kewajiban atas setiap muslim yang mendengarnya untuk mentasymitnya. Adapun menguap, maka dia tidaklah datang kecuali dari setan. Karenanya hendaklah dia menahan menguap semampunya. Jika dia sampai mengucapkan ‘haaah’, maka setan akan menertawainya.” (HR. Al-Bukhari no. 6223 dan Muslim no. 2994)'},
        {title:'SOMBONG SIFAT PENGHUNI NERAKA', content:'Haritsah bin Wahab radhiallahu anhu berkata: Saya pernah mendengar Nabi Shallallahu ‘alaihi wasallam bertanya: "“Maukah aku beritahukan kepada kalian siapa penghuni neraka?” Mereka menjawab, “Mau.” Beliau bersabda, “Setiap orang yang kasar, congkak dalam berjalan, dan sombong.” (HR. Al-Bukhari no. 9417 dan Muslim no. 2853)'},
        {title:'Apa ciri orang yg beruntung', content:'Beruntunglah orang yang (1) mendapatkan hidayah ke dalam Islam, (2) hidup cukup dan (3) puas dengan apa yang ada (HR.Tirmidzi dan Hakim)'}
    ],
    responseSurah: [],
    responseSurahTranslation: [],
    current:{},
    jadwalSholat:{},
    loadListAllSurah: function (){
  
        return  m.request({
                    method:"GET",
                    url:'http://api.alquran.cloud/quran/id.indonesian',
                    withCredentials:false,
                })
                .then(function(response){
                    QuranAPI.isLoading = false;
                    QuranAPI.response = response;

                });
    },
    loadSurah: function (surahNumber){
        QuranAPI.responseSurah = [];
        QuranAPI.isLoading = true;
   
        return  m.request({
                    method:"GET",
                    url:'http://api.alquran.cloud/surah/'+surahNumber+'/quran-uthmani',
                    withCredentials:false,
                })
                .then(function(response){
                    QuranAPI.responseSurah = response.data.ayahs;
                    var temp = QuranAPI.responseSurah[0].text;        
                    var r;
                    console.log((temp.indexOf('بِسْمِ ٱللَّهِ ٱلرَّحْمَٰنِ ٱلرَّحِيمِ') > -1) );
                    if((temp.indexOf('بِسْمِ ٱللَّهِ ٱلرَّحْمَٰنِ ٱلرَّحِيمِ') > -1) && (surahNumber !== 1))
                    {
                        r = temp.replace('بِسْمِ ٱللَّهِ ٱلرَّحْمَٰنِ ٱلرَّحِيمِ', '');
                        QuranAPI.responseSurah[0].text = r;
                        
                    }
                    QuranAPI.loadSurahTranslation(surahNumber);
                    
                    //setTimeout(QuranLayout.scrollToAyat, 1000)
                    
                });
    },
    loadSurahTranslation: function (surahNumber){
        QuranAPI.responseSurahTranslation = [];
        QuranAPI.isLoading = true;

        
        return  m.request({
                    method:"GET",
                    url:'http://api.alquran.cloud/surah/'+surahNumber+'/id.indonesian',
                    withCredentials:false,
                })
                .then(function(response){
                    
                    QuranAPI.responseSurahTranslation = response.data.ayahs;
                    QuranAPI.isLoading = false;
                    QuranAPI.responseSurah.unshift({text:'بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيم', numberInSurah:0});
                    QuranAPI.responseSurahTranslation.unshift({text:'', numberInSurah:0});                    
                });
    },
    getJadwalSholat:function(location){

        return  m.request({
                    method:"GET",
                    url:'http://muslimsalat.com/'+location+'/daily.json',
                    withCredentials:false,
                })
                .then(function(response){
                    QuranAPI.jadwalSholat = response.items[0];
                    //myApp.hideIndicator();
                });
    },
    searchJadwalSholat:function(query){
        
    },
    loadMore:function(){

    }
}

module.exports = QuranAPI;