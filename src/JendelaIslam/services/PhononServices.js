var QuranAPI = require('../services/QuranAPI');

var PhononServices = {

    init:function(pages, defaultPage){
        phonon.options({
            navigator: {
                defaultPage: defaultPage,
                animatePages: true,
                enableBrowserBackButton: true,
                useHash:true
            },
            i18n: null // for this example, we do not use internationalization
        });

        var app = phonon.navigator();

        /**
         * The activity scope is not mandatory.
         * For the home page, we do not need to perform actions during
         * page events such as onCreate, onReady, etc
        */
        pages.map(function(val, idx){
            app.on({page: val, preventClose: false, content: null, readyDelay:1}, function(activity){
                
                activity.onReady(function(){
                    switch (val) {
                        case 'readpage':
                                QuranAPI.loadSurah(QuranAPI.current.suratNumber);
                            break;
                    
                        default:
                            break;
                    }
                });

                activity.onClose(function(self) {
                    console.log(self);
                });

                activity.onHidden(function() {
                    console.log('hidden');
                });

                activity.onHashChanged(function(param1, param2) {                    
                    switch (val) {
                        case 'readpage':
                                QuranAPI.current.title = param1;
                                QuranAPI.current.suratNumber = param2;
                                QuranAPI.current.suratName = param1;       
                            break;
                    
                        default:
                            break;
                    }
                });

            });

        });
        
        app.start();
    }
}

module.exports = PhononServices;