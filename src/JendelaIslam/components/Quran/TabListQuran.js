var m = require("mithril");
var { Tabs, Tab,
    TabContent } = require('../../../view/components/PhononComponents');

var TabListQuran = {
    view:function(vnode){
        return m(Tabs, [
                    m(Tab, 'SURAH'),
                    m(Tab, 'JUZ'),
                    m(Tab, 'BOOKMARK'),
            ])
    }
}

module.exports = TabListQuran;