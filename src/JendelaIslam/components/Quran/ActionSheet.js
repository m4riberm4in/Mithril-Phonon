var m = require("mithril");
var QuranAPI = require('../../services/QuranAPI');
var { Panel, List } = require('../../../view/components/PhononComponents');

var ActionSheet = {
    oninit:function(){
        this.currentAyat = '';
        this.currentSuratName = '';
    },

    playAllAudio:function(ayat){
        $$('span.font-mequran').css('color', '');
        var audio = document.getElementById("audio-player");
        audio.firstChild.setAttribute("src", QuranAPI.current.srcAudio);
        audio.load(); //call this to just preload the audio without playing
        audio.play(); //call this to play the song right away
        audio.onplay = function(){
            console.log('play');
            //QuranLayout.isPlay = true;
            //$$("#ayah_"+ayat).css('color', 'green');
        }
        audio.onended = function(){
            console.log('end');
            //$$("#ayah_"+ayat).css('color', '');
            //$$("#ayah_"+ayat).attr('color', '');
            nextAyat = $$("#ayah_"+ayat).attr('data-ayat');
            if(ayat < QuranAPI.responseSurah.length)
            {
                ayat++;
                nextAyat++;
                QuranLayout.srcAudio = 'http://cdn.alquran.cloud/media/audio/ayah/ar.muhammadjibreel/'+nextAyat+'/low';
                QuranLayout.playAllAudio(ayat);
            }
        }
    },
    stopAudio:function(){
        $$('span.font-mequran').css('color', '');
        var audio = $$('#audio-player')[0];
        audio.pause();
    },
    
    bookmark:function(suratName, suratNumber, ayat){
        if(typeof(Storage) !== undefined)
        {
            localStorage.setItem('suratName', suratName);
            localStorage.setItem('suratNumber', suratNumber);
            localStorage.setItem('ayat', ayat);
            phonon.alert('Tandai Ayat Berhasil');
        }
        else
        {
            phonon.alert('No Web Storage Support');
        }
    },

    view:function(vnode){
        let {title, playAudio, isPlayAudio} = vnode.attrs;
        let {currentAyat, currentSuratName} = vnode.state;
        title = 'QS:'+currentSuratName+' Ayat : '+currentAyat
        const playEl = (!isPlayAudio) ? [ 
            m('a.pull-left.fa.fa-play'), m('span.padded-list', 'Play Murottal')
            ] : [ m('a.pull-left.fa.fa-stop'), m('span.padded-list', 'Stop Audio')]
        return m(Panel, vnode.attrs, 
                    m(List, [
                        [
                            m('div', {onclick:playAudio}, playEl)
                        ],
                        [
                            m('a.pull-left.fa.fa-play-circle'),
                            m('span.padded-list', 'Play Murottal Semua Ayat')
                        ],
                        [
                            m('a.pull-left.fa.fa-bookmark'),
                            m('span.padded-list', 'Tandai Ayat')
                        ],
                        [
                            m('a.pull-left.fa.fa-share'),
                            m('span.padded-list', 'Share')
                        ],
                    ])
                )
    }
}

module.exports = ActionSheet;