var m = require("mithril");
var QuranAPI = require('../../services/QuranAPI');
var { TabList, List, Content,
    CardContent, TabContent,
    Card } = require('../../../view/components/PhononComponents');

var ContentQuran = {
    changePage:function(params){
        phonon.navigator().changePage(params.page, params.param);
    },
    view:function(vnode){
        return m(Content, {'data-tab-contents':'true', 'data-disable-swipe':'false', 'data-tab-default':1}, 
        [
            m(TabContent, 
                 m(List, QuranAPI.listAyat.map(function(val, idx){
                        return [
                                m('a.pull-left', idx+1), 
                                m('a.pull-right.icon.icon-chevron-right'),
                                m('a.padded-list', {onclick:ContentQuran.changePage.bind(this, {
                                    page:'readpage',
                                    param:val+"/"+(idx+1)
                                })}, val),
                            ]                        
                    })
                ),
            ),
            m(TabContent, [
                m(List, QuranAPI.listJuz.map(function(val, idx){
                    return [
                            m('a.pull-left', idx+1), 
                            m('a.pull-right.icon.icon-chevron-right'),
                            m('a.padded-list', {href:"#!readpage/"+val}, val),
                        ]                        
                })
            ),
            ]),
            m(TabContent, {class:'background-image'},[
                m('.Padded-full', 
                    m(Card, 
                    m(CardContent, [
                        m('p', 'Q.S Surat Al - Fatihah'),
                    ]),
                ))
            ]),
        ])
    }
}

module.exports = ContentQuran;