var m = require("mithril");
var { Header, HeaderLink } = require('../../view/components/PhononComponents');

var GlobalHeader = {
    view:function(vnode){

        return m(Header, vnode.attrs, [
                m(HeaderLink, {dir:'left', icon:vnode.attrs.iconLeft, onclick:vnode.attrs.onclickLeft, 'data-navigation':"$previous-page"}),
                m(HeaderLink, {dir:'right', icon:vnode.attrs.iconRight, onclick:vnode.attrs.onclickRight}),
        ])
    }
}

module.exports = GlobalHeader;