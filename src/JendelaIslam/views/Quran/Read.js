var m = require('mithril');
var QuranAPI = require('../../services/QuranAPI');
var Header = require('../../components/GlobalHeader');
var { SearchBar, Content, List, Spinner, VirtualList } = require('../../../view/components/PhononComponents');

var Read = {
    oninit:function(){
        this.title = '';
        this.isSearch = false;
        this.isLoading = false;
    },
    oncreate:function(vnode){

    },
    scrollToAyat:function(){
        if(QuranAPI.isScroll)
        {
            var el = $$("#ayat_list_"+localStorage.getItem('ayat'));
            var page = $$(".page-content.surat");
            console.log($$("#ayat_list_1").offset().top)
            var newpos = el.offset().top - ($$("#ayat_list_1").offset().top - 80);
            page.scrollTop(newpos);
            QuranAPI.isScroll = false;
        }
    },    

    searchClick:function(vnode, event){
        vnode.state.isSearch = !vnode.state.isSearch;
    },
    view:function(vnode){
        const {openActionSheet} = vnode.attrs;
        let arabic = QuranAPI.responseSurah;
        let translation = QuranAPI.responseSurahTranslation;
        const rows = [];
        


        //config for virtual list
        const config = {
            width: '100%',
            height:window.innerHeight,
            itemHeight: 100, 
            total: arabic.length, //total of the items
            generate(index) { //this genereate function for the element
                    let item;
                    (index === 0) ?
                        item = '<div class="padded-full"><h2 class="font-mequran text-center">'+arabic[index].text+'</h2></div>' :
                        item = generateTemplateItem({index:index, arabic:arabic, translation:translation}); //generate string element
                    const el = document.createElement('li'); //create the container using li
                    el.setAttribute('id', 'ayah-'+index);
                    el.setAttribute('data-index', index);
                    (index % 2 === 0) ? el.style = 'background-color:floralwhite;opacity:0.9;' : el.style = 'background-color:white;opacity:0.9;';
                    el.innerHTML = item; //add chil
                    el.style.width = '100%';
                    el.onclick = openActionSheet.bind(this, vnode, arabic[index]);
                    return {element:el, height:rows[index].height}; //set the dynamic height based on the rendered dom height, so we will get the dynamic height
                }
        };
        
        //generate temporary element to get the height renderer dom element
        const generateHeightTemplateItem = (size, container, rows) => {
            for(let i = 0; i < size; i++)
            {
                let item;
                (i === 0) ?
                    item = '<div class="padded-full"><h2 class="font-mequran text-center">'+arabic[i].text+'</h2></div>' :
                    item = generateTemplateItem({index:i, arabic:arabic, translation:translation}); //generate string element
                const el = document.createElement('li');                
                el.setAttribute('data-index', i);
                el.innerHTML = item;
                el.style.width = '100%';
                 
                //el.style.visibility = "hidden";
                container.appendChild(el); //render to DOM
                rows.push(Object.assign({}, {height:el.clientHeight})); //store the height
                container.removeChild(el); //remove the element from DOM
            }
        }

        // this function needed to get the actual height after render to dom, then we store the height
        const generateTemplateItem = (data) => { 
            let template = ''
            if(data.translation.length > 0) 
            {
                QuranAPI.isLoading = false;
                
                const a = '<div class="phone-2 tablet-2 column"><div class="padded-list">'+(data.index)+'</div></div>';
                const b = '<div class="phone-8 tablet-8 column"><div class="font-mequran rtl style-ayah">'
                    +data.arabic[data.index].text+
                    '</div><span class="body">'+data.translation[data.index].text+'</span></div>';
                const c = '<div class="phone-2 tablet-2 column"><div class="pull-right icon icon-chevron-right"></div></div>';
                const child = '<div class="row"> '+a+b+c+' </div>';
                template = child;
                 
            }
            else
            {
                QuranAPI.isLoading = true;
            }
            
            return template;

        };

        let {isLoading, isSearch, title} = vnode.state;
        isLoading = QuranAPI.isLoading;
        title = vnode.attrs.title;

        const Head = (isSearch) ? 
        m(SearchBar, {placeholder:'search', onclickCancel:Read.searchClick.bind(this, vnode)}) :
        m(Header, {title:title, 
                iconLeft:'icon-arrow-back', 
                iconRight:'fa fa-search', 
                onclickLeft:function(){
                },
                onclickRight:Read.searchClick.bind(this, vnode)});
        
        var Audio = m("div", m("audio", {id:"audio-player"}, m('source', {src:'', type:'audio/mp3'})));
        var Bismillah = m('.padded-full', m('h2.font-mequran.text-center','بِسْمِ اللَّهِ الرَّحْمَٰنِ الرَّحِيمِ'))

        return m('readpage', {'data-page':true, 'data-loader':true},[
                Head,
                Audio,
                m(Content, {class:'background-image'}, [
                        //Bismillah,
                        (QuranAPI.isLoading) ? 
                        m(Spinner) :
                        m(VirtualList, 
                            {
                                rows:rows,
                                size:arabic.length,
                                config:config,
                                generateHeightTemplateItem:generateHeightTemplateItem,
                            }),
                        /*m(List, QuranAPI.responseSurah.map(function(val, idx){
                            var surahTranslation = QuranAPI.responseSurahTranslation[idx].text;
                            
                            return [
                                    m('a.row', (idx % 2 === 0) ? { style:'background-color:floralwhite;'} : {}, [
                                        m('.phone-2.tablet-2.column', {style:'text-align:center;'}, 
                                            m('div', idx+1)
                                        ),
                                        m('.phone-8.tablet-8.column', {onclick:Read.openActionSheet.bind(this, val)},
                                            m('div', [
                                                m('p.font-mequran.rtl.style-ayah', val.text),
                                                m('span.body.align-right', surahTranslation)
                                            ])
                                        ),
                                        m('.phone-2.tablet-2.column', m('a.pull-right.icon.icon-chevron-right')),
                                    ])
                                ]                        
                        })) */
                    ]) 
                ])
    }
}

module.exports = Read;