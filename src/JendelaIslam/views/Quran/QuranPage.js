var m = require('mithril');
var Header = require('../../components/GlobalHeader');

var TabListQuran = require('../../components/Quran/TabListQuran');
var ContentQuran = require('../../components/Quran/ContentQuran');
var ActionSheet = require('../../components/Quran/ActionSheet');
var Read = require('../../views/Quran/Read');
var QuranAPI = require('../../services/QuranAPI');

var { SearchBar, TabContent, Panel } = require('../../../view/components/PhononComponents');


var QuranHome = {
    currentElementAyat:{},
    isPlayAudio:false,
    oninit:function(){
        this.isSearch = false;
        this.currentAyat = '';
        this.currentSuratName = QuranAPI.current.suratName;
    },
    oncreate:function(vnode){
        vnode.state.currentAyat = QuranAPI.current.ayat;
        vnode.state.currentSuratName = QuranAPI.current.suratName;
    },
    searchClick:function(vnode, event){
        vnode.state.isSearch = !vnode.state.isSearch;
    },
    stopAudio:function(audio, currentElAyat){
        QuranHome.isPlayAudio = false;
        currentElAyat.style.color = '';
        audio.pause();
        phonon.panel("#ayah-panel").close()
    },
    playAudio:function(){
        var containerElementAyat = QuranHome.currentElementAyat.parentElement;
        var nextSiblingEl =  QuranHome.currentElementAyat.nextSibling;
        var audio = document.getElementById("audio-player");

        audio.firstChild.setAttribute("src", QuranAPI.current.srcAudio);
        audio.load(); //call this to just preload the audio without playing
        (QuranHome.isPlayAudio) ? QuranHome.stopAudio(audio, containerElementAyat) : audio.play(); //call this to play the song right away
        
        audio.onplay = function(){
            console.log('play');
            phonon.panel("#ayah-panel").close();
            containerElementAyat.style.color = 'green';
            //nextSiblingEl.style.color = 'green';
            QuranHome.isPlayAudio = true;
        }
        audio.onended = function(){
            console.log('end');
            containerElementAyat.style.color = '';
            //nextSiblingEl.style.color = '';
            QuranHome.isPlayAudio = false;
        }
    },
    openActionSheet:function(vnode, val, event){

        QuranHome.currentElementAyat = event.target;
        QuranAPI.current.srcAudio = 'http://cdn.alquran.cloud/media/audio/ayah/ar.abdulsamad/'+val.number+'/low';
        QuranAPI.current.ayat = val.numberInSurah;
        var panel = document.getElementById('ayah-panel');
        phonon.panel('#ayah-panel').open();
        m.redraw();
    },
    view:function(vnode){
        var {isSearch, currentAyat, currentSuratName} = vnode.state;
        currentAyat = QuranAPI.current.ayat;
        currentSuratName = QuranAPI.current.suratName;

        var Head = (isSearch) ? 
            m(SearchBar, {placeholder:'search', onclickCancel:QuranHome.searchClick.bind(this, vnode)}) :
            m(Header, {title:'Alquran Indonesia', 
                    iconLeft:'icon-arrow-back', 
                    iconRight:'fa fa-search', 
                    onclickLeft:function(){
                        
                    },
                    onclickRight:QuranHome.searchClick.bind(this, vnode)});
            
        return [
                m(ActionSheet, {isPlayAudio:QuranHome.isPlayAudio, playAudio:QuranHome.playAudio, id:'ayah-panel', title:'QS:'+currentSuratName+' Ayat : '+currentAyat}), 
                m('quranpage', {'data-page':true}, [
                Head,
                m(TabListQuran),
                m(ContentQuran),
                m(Read, {title:currentSuratName, openActionSheet:QuranHome.openActionSheet})
            ])
        ]
    }
}

module.exports = QuranHome;