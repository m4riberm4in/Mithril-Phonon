var m = require('mithril');
var QuranHome = require('./Quran/QuranPage');
var PhononServices = require('../services/PhononServices');

var Home = {
    oncreate:function(){
        PhononServices.init(['quranpage', 'readpage'], 'quranpage');
    },
    view:function(vnode){
        return m(QuranHome)
    }
}

module.exports = Home;