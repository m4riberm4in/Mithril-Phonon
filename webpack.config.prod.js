var path = require("path");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const JavaScriptObfuscator = require('webpack-obfuscator');



module.exports = {
  entry: {
    'app': "./src/index.js"
  },
  output: {
    path: path.resolve(__dirname, "js"),
    publicPath: "/js/",
    filename: '[name].js'
  },
  plugins: [
    new UglifyJSPlugin({
      include: /\.min\.js$/,
      minimize: true,
      sourceMap: false,
      output: {
        comments: false
      },
      compressor: {
        warnings: false
      },
    }),
    new JavaScriptObfuscator({
      rotateUnicodeArray: true
    })
  ],
};